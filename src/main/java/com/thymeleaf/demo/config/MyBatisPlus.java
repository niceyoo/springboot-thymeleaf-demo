package com.thymeleaf.demo.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author : niceyoo
 * @version : 1.0
 * @title : MyBatisPlus.java
 * @description : mybatisplus配置类
 * @copyright : 公共服务与应急管理战略本部 Copyright(c)2020
 * @date : 2021/7/15 14:32
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.thymeleaf.demo.dao")
public class MyBatisPlus {

    /**
     * myabtis 实现的分页为什么还要分页插件？
     * <p>
     * 1.mybatis实现得分页时逻辑分页或者叫做内存不是物理分页
     * 2.他是把符合条件的数据全部查询出来放到内存中，然后返回你需要的那部分
     * 3.表中数据不多时,可以使用,速度慢一些;当数据量大时，建议使用物理分页
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
      return new PaginationInterceptor();
    }

}
