package com.thymeleaf.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.thymeleaf.demo.dao.UserDao;
import com.thymeleaf.demo.entity.User;
import com.thymeleaf.demo.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author : niceyoo
 * @version : 1.0
 * @title : UserServiceImpl.java
 * @description : User表服务实现类
 * @copyright : 公共服务与应急管理战略本部 Copyright(c)2020
 * @date : 2021/7/15 14:32
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

}
