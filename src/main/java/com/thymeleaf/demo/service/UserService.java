package com.thymeleaf.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.thymeleaf.demo.entity.User;

/**
 * @author : niceyoo
 * @version : 1.0
 * @title : UserService.java
 * @description : User表服务接口
 * @copyright : 公共服务与应急管理战略本部 Copyright(c)2020
 * @date : 2021/7/15 14:32
 */
public interface UserService extends IService<User> {

}
