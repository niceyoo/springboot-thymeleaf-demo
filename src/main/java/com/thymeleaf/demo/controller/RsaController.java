package com.thymeleaf.demo.controller;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.thymeleaf.demo.utils.RSAUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * @author : niceyoo
 * @version : 1.0
 * @title : RSAController.java
 * @description : rsa控制层
 * @copyright : 公共服务与应急管理战略本部 Copyright(c)2020
 * @date : 2021/7/15 14:32
 */
@RestController
@RequestMapping("rsa")
public class RsaController extends ApiController {

    /**
     * 声明加密秘钥
     */
    private static String m;

    /**
     * 声明加密模块
     */
    private static String mou;

    /**
     * 用户列表展示
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    @GetMapping("view")
    public ModelAndView selectAll() throws NoSuchAlgorithmException {
        Map jmInfo = RSAUtils.getModulus();
        return new ModelAndView("/rsa").addObject("data",jmInfo);
    }

    /**
     * 获取key
     *
     * @return
     * @throws NoSuchAlgorithmException
     */
    @GetMapping("key")
    public R key() throws NoSuchAlgorithmException {
        Map jmInfo = RSAUtils.getModulus();
        String my = jmInfo.get("m").toString();
        m = my;
        mou = jmInfo.get("modu").toString();
        System.out.println("1111");
        return success(jmInfo);
    }

    /**
     * 解密
     *
     * @param request
     * @return
     * @throws NoSuchAlgorithmException
     */
    @GetMapping("decryption")
    public R decryption(HttpServletRequest request) throws Exception {
        String encryptStr = request.getParameter("encryptStr");
        System.out.println("前端加密："+encryptStr);
        String decrypt = RSAUtils.Decrypt(encryptStr, mou, m);
        System.out.println(decrypt);
        return success(decrypt);
    }

}
