package com.thymeleaf.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thymeleaf.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author : niceyoo
 * @version : 1.0
 * @title : UserDao.java
 * @description : 表数据库访问层
 * @copyright : 公共服务与应急管理战略本部 Copyright(c)2020
 * @date : 2021/7/15 14:32
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

}
