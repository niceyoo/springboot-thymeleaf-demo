let table = null;
let form = null;
let layer = null;
let laydate = null;
layui.use(['table', 'form', 'layer', 'laydate'], function () {
    $(function () {
        table = layui.table;
        form = layui.form;
        layer = layui.layer;
        laydate = layui.laydate;
        loadUser();
        //查询
        $('#query').click(function (e) {
            let username = $('#username').val();
            loadUser(username);
        });
    });

    //todo 监听事件
    table.on('toolbar(objTableFilter)', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data; //获取选中的数据
        switch (obj.event) {
            case 'add':
                layer.open({
                    id: 'add',
                    type: 1,
                    title: ['新增用户'],
                    skin: 'layui-layer-molv',
                    area: '500px',
                    offset: 'auto',
                    content:
                        '<div class="layui-row"  style="margin-top:10px;">' +
                        '    <div class="layui-col-md10">' +
                        '        <form class="layui-form" id="addForm">' +
                        '            <div class="layui-form-item">\n' +
                        '                <label class="layui-form-label" style="padding-left:-50px;">用户名:</label>\n' +
                        '                <div class="layui-input-block">\n' +
                        '                    <input type="text" placeholder="请输入用户名" id="userName" class="layui-input" th:text="${data.modu}">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="layui-form-item">\n' +
                        '                <label class="layui-form-label" style="padding-left:-50px;">昵称:</label>\n' +
                        '                <div class="layui-input-block">\n' +
                        '                    <input type="text" placeholder="请输入用户昵称" id="userNickName" class="layui-input">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <div class="layui-form-item">\n' +
                        '                <label class="layui-form-label" style="padding-left:-50px;">年龄:</label>\n' +
                        '                <div class="layui-input-block">\n' +
                        '                    <input type="text" placeholder="请输入年龄" id="age" class="layui-input">\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '        </form>\n' +
                        '    </div>\n' +
                        '</div>\n',
                    btn: ['提交', '取消']
                    , success: function (layero) {
                        layero.find('.layui-layer-btn').css('text-align', 'center');
                    },
                    btn1: function (index) {
                        // 数据校验
                        let userName = $('#addForm #userName').val();
                        if (userName == null || userName == '') {
                            layer.msg('用户名不能为空', {icon: 5, time: 1500});
                            return;
                        }
                        let userNickName = $('#addForm #userNickName').val();
                        if (userNickName == null || userNickName == '') {
                            layer.msg('用户昵称不能为空', {icon: 5, time: 1500});
                            return;
                        }
                        let age = $('#addForm #age').val();
                        if (age == null || age == '') {
                            layer.msg('年龄不能为空', {icon: 5, time: 1500});
                            return;
                        }
                        let obj = {
                            userName: userName,
                            nickName: userNickName,
                            age: age,

                        };
                        ajaxAsync(JSON.stringify(obj), '/user/insert', function (res) {
                            if (res.data == true) {
                                layer.msg('保存成功', {icon: 1, time: 1500}, function () {
                                    layer.closeAll();
                                    table.reload('userList');
                                });
                            } else {
                                layer.alert('保存失败', {icon: 5, time: 1500});
                            }
                        });
                    },
                    btn2: function (index, layero) {
                        layer.close(index);
                    }
                });
                break;
            case 'delete':
                if (data.length == 0) {
                    layer.msg('请选择一行', {icon: 1, time: 1500});
                } else {
                    let length = data.length;
                    let obj = new Array();
                    for (let i = 0; i < length; i++) {
                        obj.push(data[i].id);
                    }
                    layer.confirm('是否删除？', {title: '提示'}, function (index) {
                        ajaxAsync(JSON.stringify(obj), '/user/delete', function (res) {
                            if (res.data == true) {
                                layer.msg('删除成功', {icon: 1, time: 1500}, function () {
                                    //关闭弹窗
                                    layer.closeAll();
                                    // 重新刷新表格
                                    table.reload('userList');
                                });
                            } else {
                                layer.msg('删除失败', {icon: 5, time: 1500});
                            }
                        });
                    });
                }
                break;
            case 'update':
                if (data.length == 0) {
                    layer.msg('请选择一行');
                } else if (data.length > 1) {
                    layer.msg('只能选择一行');
                } else {
                    layer.open({
                        id: 'update',
                        type: 1,
                        title: ['修改用户'],
                        skin: 'layui-layer-molv',
                        area: '500px',
                        offset: 'auto',
                        content:
                            '<div class="layui-row"  style="margin-top:10px;">' +
                            '    <div class="layui-col-md10">' +
                            '        <form class="layui-form" id="updateForm">' +
                            '            <div class="layui-form-item">\n' +
                            '                <label class="layui-form-label" style="padding-left:-50px;">用户名:</label>\n' +
                            '                <div class="layui-input-block">\n' +
                            '                    <input type="text" placeholder="请输入用户名" id="userName" class="layui-input">\n' +
                            '                </div>\n' +
                            '            </div>\n' +
                            '            <div class="layui-form-item">\n' +
                            '                <label class="layui-form-label" style="padding-left:-50px;">昵称:</label>\n' +
                            '                <div class="layui-input-block">\n' +
                            '                    <input type="text" placeholder="请输入用户昵称" id="nickName" class="layui-input">\n' +
                            '                </div>\n' +
                            '            </div>\n' +
                            '            <div class="layui-form-item">\n' +
                            '                <label class="layui-form-label" style="padding-left:-50px;">年龄:</label>\n' +
                            '                <div class="layui-input-block">\n' +
                            '                    <input type="text" placeholder="请输入年龄" id="age" class="layui-input">\n' +
                            '                </div>\n' +
                            '            </div>\n' +
                            '        </form>\n' +
                            '    </div>\n' +
                            '</div>\n',
                        btn: ['提交', '取消']
                        , success: function (layero) {
                            // 展示在弹出层里面
                            $('#updateForm #userName').val(data[0].userName);
                            $('#updateForm #nickName').val(data[0].nickName);
                            $('#updateForm #age').val(data[0].age);
                            layero.find('.layui-layer-btn').css('text-align', 'center');
                        },
                        btn1: function (index) {
                            // 数据校验
                            let userName = $('#updateForm #userName').val();
                            if (userName == null || userName == '') {
                                layer.msg('用户名不能为空', {icon: 1, time: 1500});
                                return;
                            }
                            let nickName = $('#updateForm #nickName').val();
                            if (nickName == null || nickName == '') {
                                layer.msg('用户昵称不能为空', {icon: 1, time: 1500});
                                return;
                            }
                            let age = $('#updateForm #age').val();
                            if (age == null || age == '') {
                                layer.msg('用户年龄不能为空', {icon: 1, time: 1500});
                                return;
                            }
                            // 提交
                            let user = {
                                nickName: nickName,
                                userName: userName,
                                age: age,
                                id: data[0].id
                            };
                            ajaxAsync(JSON.stringify(user), '/user/update', function (res) {
                                if (res.data == true) {
                                    layer.msg('修改成功', {icon: 1, time: 1500}, function () {
                                        layer.closeAll();
                                        table.reload('userList');
                                    });
                                } else {
                                    layer.alert('修改失败', {icon: 5, time: 1500});
                                }
                            });
                        },
                        btn2: function (index, layero) {
                            layer.close(index);
                        }
                    });
                }
        }
    });
});

/**
 * todo 加载显示数据列表
 * @param table
 * @param empParam
 */
function loadUser(userName) {
    //员工列表
    table.render({
        id: 'userList',
        elem: '#userTable'
        , url: '/user/selectAll'
        , toolbar: '#toolbar'
        , page: true
        , method: 'get'
        , size: 'sm'
        ,where:{
            userName:userName
        }
        , request: {
            pageName: 'current', //页码的参数名称，默认：page
            limitName: 'size' //每页数据量的参数名，默认：limit
        }
        , defaultToolbar: []
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.records //解析数据列表
            };
        }
        , cols: [
            [ //表头
                {field: 'number', type: 'numbers'}
                , {field: 'checkbox', type: 'checkbox'}
                , {field: 'userName', title: '用户名', width: '20%', align: 'center'}
                , {field: 'nickName', title: '昵称', width: '20%', align: 'center'}
                , {field: 'age', title: '年龄', width: '20%', align: 'center'}
                , {field: 'createTime', title: '创建时间', width: '20%', align: 'center'}
            ]
        ]
    });
}

/**
 * todo 统一调用ajax方法
 * @param data
 * @param url
 * @param callback
 */
function ajaxAsync(data, url, callback) {
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        success: callback
    });
}

